import { Component, OnInit, NgZone } from '@angular/core';
import { Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

import { UsuarioProvider } from '../providers/usuario/usuario';
import { Credenciales } from '../interfaces/credenciales';

import { HomePage } from '../pages/index.paginas';

import "rxjs/add/operator/filter";
import "rxjs/add/operator/first";

@Component({
  templateUrl: 'app.html'
})
export class MyApp /*implements OnInit*/{
  rootPage:any /*= "LoginPage"*/;
  mostrarMenu: boolean = false;
  dataUser: Credenciales;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private afAuth: AngularFireAuth,
    public _usuarioProv: UsuarioProvider,
    private menuCtrl: MenuController,
    private zone: NgZone ) {
      platform.ready().then(() => {

        this.afAuth.authState.subscribe( user => {
          if (user) {
            this._usuarioProv.cargarUsuario(user.displayName, user.email, user.photoURL, user.uid, user.providerId);
            this.dataUser = this._usuarioProv.usuario;
            this.mostrarMenu = true;
            zone.run(() => {
              this.rootPage = HomePage;
            });
          }else{
            zone.run(() => {
              this.rootPage = "LoginPage";
            });
          }
        });

        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        statusBar.styleDefault();
        splashScreen.hide();
      });
  }

  cerrarSesion() {

    this.afAuth.auth.signOut().then( (res: any) => {
       this._usuarioProv.usuario = {};
       this.dataUser = {};
       this.zone.run(() => {
         this.rootPage = "LoginPage";
       });
       this.mostrarMenu = false;

     });
  }

  // ngOnInit(){
  //   console.log("ngOnInit");
  //   this.dataUser = this._usuarioProv.usuario;
  // }


}
