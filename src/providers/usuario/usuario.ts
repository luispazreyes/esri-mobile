import { Injectable } from '@angular/core';
import { Credenciales } from '../../interfaces/credenciales';

import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';

@Injectable()
export class UsuarioProvider {

  usuario: Credenciales = {};
  ajustes = {
    mostrar_tutorial: true
  }

  constructor( private storage: Storage, private platform: Platform ) { }


  cargarUsuario( nombre: string,
                 email: string,
                 imagen: string,
                 uid: string,
                 provider: string ) {

    this.usuario.nombre = nombre;
    this.usuario.email = email;
    this.usuario.imagen = imagen;
    this.usuario.uid = uid;
    this.usuario.provider = provider;

  }

  cargarStorage(){

    let promesa = new Promise( (resolve, reject) => {
      if (this.platform.is('cordova')) {
        // Dispositivo

        this.storage.ready()
          .then( () => {
            this.storage.get('ajustes')
              .then( (ajustes) => {
                if (ajustes) {
                  console.log('En el usuario', ajustes);
                  this.ajustes = ajustes;
                }

                resolve();
              });
          });

      }else{
        // Escritorio
        if ( localStorage.getItem('ajustes') ) {
          this.ajustes = JSON.parse(localStorage.getItem('ajustes'));
        }

        resolve();
      }
    });

    return promesa;

  }

  guardarStorage(){
    if (this.platform.is('cordova')) {
      // Dispositivo
      this.storage.set( 'ajustes', this.ajustes );
    }else{
      // Escritorio
      localStorage.setItem( 'ajustes',JSON.stringify(this.ajustes) );
    }
  }
}
