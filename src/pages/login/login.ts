import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// Anfular Fire 2
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

// Pages
import { HomePage } from '../home/home';

// Plugin Native
import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

// providers
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'LoginPage',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private afAuth: AngularFireAuth,
    private fb: Facebook,
    private platform: Platform,
    public _usuarioProv: UsuarioProvider,
    private googlePlus: GooglePlus ) {
  }

  signInGoogle() {

    this.googlePlus.login({
      'webClientId': '808509392338-aa9vakfrcfpgfhesbi4pbb9hebi8pr6m.apps.googleusercontent.com',
      'offline': true
    }).then( res => {

      firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
      .then( user => {
              this._usuarioProv.cargarUsuario(
                user.displayName,
                user.email,
                user.photoURL,
                user.uid,
                'google'
              );

              //this.navCtrl.setRoot(HomePage);
      })
      .catch( error => console.log("Firebase failure: " + JSON.stringify(error)));
    }).catch(err => console.error("Error: " + JSON.stringify(err))) ;


  }

  signInWithFacebook() {

    if (this.platform.is('cordova')) {
      // celular
      this.fb.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        firebase.auth().signInWithCredential(facebookCredential)
            .then( user => {

              console.log(user);

              this._usuarioProv.cargarUsuario(
                user.displayName,
                user.email,
                user.photoURL,
                user.uid,
                'facebook'
              );

              //this.navCtrl.setRoot(HomePage);


            }).catch(e => console.log('Error con el login' + JSON.stringify(e)));
      })


    }else {
      // escritorio
      this.afAuth.auth
        .signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(res => {

          console.log(res);
          let user = res.user;

          this._usuarioProv.cargarUsuario(
            user.displayName,
            user.email,
            user.photoURL,
            user.uid,
            'facebook'
          );

          //this.navCtrl.setRoot(HomePage);

        });
    }


  }

}
