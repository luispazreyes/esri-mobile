import { Component, OnInit } from '@angular/core';
import { EsriLoaderService } from 'angular2-esri-loader';
import { NavController, MenuController, AlertController, Platform } from 'ionic-angular';

// Providers
import { UsuarioProvider } from '../../providers/usuario/usuario';

@Component({
  selector: 'HomePage',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  map: any;
  inputName: string;
  inputDescription: string;
  inputSex: string;
  locateWidget: any;

  constructor(public navCtrl: NavController, public esriLoader:EsriLoaderService, private menuCtrl: MenuController, private alertCtrl: AlertController, private platform: Platform, public _up: UsuarioProvider ) {

  }

  mostrarAlerta(){

    this._up.cargarStorage().then( () => {
      console.log('En el home', this._up.ajustes.mostrar_tutorial);
      if ( this._up.ajustes.mostrar_tutorial) {
        this.alertCtrl.create({
          title: 'Toque para crear punto',
          subTitle: 'Toque cualquier parte de la pantalla para crear un punto',
          buttons: [ {text: 'OK',
            handler: () =>{
              this._up.ajustes.mostrar_tutorial = false;
              this._up.guardarStorage();
            }
          }]
        }).present();
      }
    });
  }

  ngOnInit(){

    const options = {
      url: 'https://js.arcgis.com/4.8/'
    };

    this.esriLoader.load(options).then(() => {
      this.esriLoader.loadModules(["esri/Map",
        "esri/views/MapView",
        "esri/layers/Layer",
        "esri/layers/FeatureLayer",
        "esri/widgets/Expand",
        "esri/widgets/BasemapToggle",
        "dojo/dom",
        "esri/widgets/Home",
        "dojo/on",
        "esri/Graphic",
        "esri/core/watchUtils",
        "esri/Viewpoint",
        "esri/widgets/Locate"])
        .then(([Map, MapView, Layer, FeatureLayer, Expand, BasemapToggle, dom,
        Home, on, Graphic, watchUtils,Viewpoint, Locate]) => {

          var editArea, attributeEditing, editFeature, updateInstructionDiv, editExpand, featureLayer;

          let myMap = new Map({
            basemap: 'hybrid'
          });

          // Create a MapView instance (for 2D viewing) and reference the map instance
          var view = new MapView({
            container: "viewMap",
            map: myMap,
            zoom: 12,
            center: [-87.192136, 14.0722751] // longitude, latitude
          });

          //add an editable featurelayer from portal
          Layer.fromPortalItem({
              portalItem: { // autocasts as new PortalItem()
                id: "efc437c0c50142ef9ef932a44f581179"
              }
            }).then(addLayer)
            .catch(handleLayerLoadError);

          let setupEditing = () => {
            // input boxes for the attribute editing
             editArea = dom.byId("editArea");
             updateInstructionDiv = dom.byId("updateInstructionDiv");
             attributeEditing = dom.byId("featureUpdateDiv");

            // *****************************************************
            // btnUpdate click event
            // update attributes of selected feature
            // *****************************************************
            on(dom.byId("btnUpdate"), "click", (event) => {
              if (editFeature) {
                editFeature.attributes["nombre"] = this.inputName;
                editFeature.attributes["descripcion"] = this.inputDescription;
                editFeature.attributes["sexo"] = this.inputSex;

                var edits = {
                  updateFeatures: [editFeature]
                };

                applyEdits(edits);
              }
            });

            // *****************************************************
            // btnAddFeature click event
            // create a new feature at the click location
            // *****************************************************
            on(dom.byId("btnAddFeature"), "click", function() {
              unselectFeature();

              on.once(view, "touchend", function(event) {
                event.stopPropagation();

                //punto
                //{"spatialReference":{"latestWkid":3857,"wkid":102100},"x":-9706272.649653224,"y":1590616.5950021222}

                if (event.mapPoint) {
                  let point = event.mapPoint.clone();
                  console.log(point);
                  point.z = undefined;
                  point.hasZ = false;

                  let newIncident = new Graphic({
                    geometry: point,
                    attributes: {}
                  });

                  var edits = {
                    addFeatures: [newIncident]
                  };

                  // for (let i = 0; i < 50000; i++) {
                  //     applyEdits(edits);
                  // }
                  applyEdits(edits);

                  // ui changes in response to creating a new feature
                  // display feature update and delete portion of the edit area
                  attributeEditing.style.display = "block";
                  updateInstructionDiv.style.display = "none";
                  dom.byId("viewMap").style.cursor = "auto";
                } else {
                  console.error("event.mapPoint is not defined");
                }
              });

              // change the view's mouse cursor once user selects
              // a new incident type to create
              dom.byId("viewMap").style.cursor = "crosshair";
              editArea.style.cursor = "auto";
            });

            // *****************************************************
            // delete button click event. ApplyEdits is called
            // with the selected feature to be deleted
            // *****************************************************
            on(dom.byId("btnDelete"), "click", function() {
              var edits = {
                deleteFeatures: [editFeature]
              };
              applyEdits(edits);
            });

            // *****************************************************
            // watch for view LOD change. Display Feature editing
            // area when view.zoom level is 14 or higher
            //.catch hide the feature editing area
            // *****************************************************
            view.when(function() {
              watchUtils.whenTrue(view, "stationary", function() {
                if (editExpand) {
                  if (view.zoom <= 7) {
                    editExpand.domNode.style.display = "none";
                  } else {
                    editExpand.domNode.style.display = "block";
                  }
                }
              });
            });
          }

          let setupView = () => {
            // set home buttone view point to initial extent
            var homeWidget = new Home({
               view: view
             });

             // adds the home widget to the top left corner of the MapView
             view.ui.add(homeWidget, "top-left");

            //Basemap Toogle widget
            var basemapToggle = new BasemapToggle({
              view: view,  // The view that provides access to the map's "streets" basemap
              nextBasemap: "streets"  // Allows for toggling to the "hybrid" basemap
            });

            view.ui.add(basemapToggle, "top-right");

            // expand widget
             editExpand = new Expand({
              expandIconClass: "esri-icon-edit",
              expandTooltip: "Expand Edit",
              expanded: false,
              view: view,
              content: editArea
            });
            view.ui.add(editExpand, "top-right");
          }

          setupEditing();
          setupView();

          let locateWidget = new Locate({
           view: view,   // Attaches the Locate button to the view
           graphic: new Graphic({
             symbol: { type: "simple-marker" }  // overwrites the default symbol used for the
             // graphic placed at the location of the user when found
           })
         });

         view.ui.add(locateWidget, "top-left");

          function addLayer(layer) {
            featureLayer = layer;
            myMap.add(layer);
          }

          function handleLayerLoadError(error) {
            console.log("Layer failed to load: ", error);
          }

          editArea = dom.byId("editArea");

          let unselectFeature = () =>{
            attributeEditing.style.display = "none";
            updateInstructionDiv.style.display = "block";

            this.inputName = null;
            this.inputDescription = null;
            // inputNombre = null;
            // inputDescripcion.value = null;
            view.graphics.removeAll();
          }

          let selectFeature = (objectId) => {
            // symbol for the selected feature on the view
            var selectionSymbol = {
              type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
              color: [0, 0, 0, 0],
              style: "square",
              size: "40px",
              outline: {
                color: [0, 255, 255, 1],
                width: "3px"
              }
            };
            var query = featureLayer.createQuery();
            query.where = featureLayer.objectIdField + " = " + objectId;

            featureLayer.queryFeatures(query).then(function(results) {
              if (results.features.length > 0) {
                editFeature = results.features[0];
                editFeature.symbol = selectionSymbol;
                view.graphics.add(editFeature);
              }
            });
          }

          let applyEdits = (params) => {

            let promesa = new Promise( (resolve, reject) => {
              unselectFeature();
              var promise = featureLayer.applyEdits(params);
              editResultsHandler(promise);
              resolve();
            });

            return promesa;


          }

          let editResultsHandler = (promise) => {
            promise
              .then(function(editsResult) {
                var extractObjectId = function(result) {
                  return result.objectId;
                };

                // get the objectId of the newly added feature
                if (editsResult.addFeatureResults.length > 0) {
                  var adds = editsResult.addFeatureResults.map(
                    extractObjectId);
                  let newIncidentId = adds[0];

                  selectFeature(newIncidentId);
                }
              })
              .catch(function(error) {
                console.log("===============================================");
                console.error("[ applyEdits ] FAILURE: ", error.code, error.name,
                  error.message);
                console.log("error = ", error);
              });
          }

          view.on("click", (event) => {
          unselectFeature();
          editExpand.expanded = true;
          view.hitTest(event).then((response) => {
            if (response.results.length > 0 && response.results[0].graphic) {

              var feature = response.results[0].graphic;
              selectFeature(feature.attributes[featureLayer.objectIdField]);

              this.inputName = feature.attributes[
                "nombre"];
              this.inputDescription = feature.attributes[
                "descripcion"];
              this.inputSex = feature.attributes[
                "sexo"];
              attributeEditing.style.display = "block";
              updateInstructionDiv.style.display = "none";
            }
          });
        });

          on(dom.byId("btnAddFeature"), "click", function() {
            locateWidget.locate();
            unselectFeature();
            on.once(view, "click", function(event) {
              event.stopPropagation();

              if (event.mapPoint) {
                let point = event.mapPoint.clone();
                point.z = undefined;
                point.hasZ = false;

                let newIncident = new Graphic({
                  geometry: point,
                  attributes: {}
                });

                var edits = {
                  addFeatures: [newIncident]
                };

                applyEdits(edits)
                  .then( () =>{

                    setTimeout( () => {
                      // ui changes in response to creating a new feature
                      // display feature update and delete portion of the edit area
                      attributeEditing.style.display = "block";
                      updateInstructionDiv.style.display = "none";
                      dom.byId("viewMap").style.cursor = "auto";
                      editExpand.expanded = true;
                    }, 2000)

                  });
              } else {
                console.error("event.mapPoint is not defined");
              }
            });

            // change the view's mouse cursor once user selects
            // a new incident type to create
            dom.byId("viewMap").style.cursor = "crosshair";
            editArea.style.cursor = "auto";
          });
        });
    });
  }
}
