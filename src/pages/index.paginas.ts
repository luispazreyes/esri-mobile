export { HomePage } from './home/home';
export { TabsPage } from './tabs/tabs';
export { MenuPage } from './menu/menu';
export { AboutPage } from './about/about';
export { ContactPage } from './contact/contact';
